# frozen_string_literal: true

class SetLocaleMiddleware
  # BEGIN
  def initialize(app)
    @app = app
  end

  def call(env)
    locale = extract_locale_from_accept_language_header env

    I18n.locale = locale

    @app.call(env)
  end

  private

  def extract_locale_from_accept_language_header(env)
    parsed_locale = env.fetch('HTTP_ACCEPT_LANGUAGE', '').scan(/^[a-z]{2}/).first

    if I18n.available_locales.map(&:to_s).include?(parsed_locale)
      parsed_locale
    else
      I18n.default_locale
    end
  end
  # END
end
