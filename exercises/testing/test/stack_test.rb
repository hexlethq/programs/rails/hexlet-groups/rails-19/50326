# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new [42, 69]
  end

  def test_stack_push
    expected = [42, 69, 322, 3, 2]
    @stack.push!(322)
    @stack.push!(3)
    @stack.push!(2)
    assert_equal expected, @stack.to_a
  end

  def test_stack_pop
    expected = [42]
    @stack.pop!
    assert_equal expected, @stack.to_a
  end

  def test_stack_size
    @stack.pop!
    assert_equal 1, @stack.size
  end

  def test_stack_clear
    @stack.clear!
    assert_equal 0, @stack.size
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
