# frozen_string_literal: true

require 'test_helper'

class VacanciesControllerTest < ActionDispatch::IntegrationTest
  test 'publish on moderate' do
    vacancy = vacancies(:on_moderate)

    patch publish_vacancy_path(vacancy)

    assert_response :redirect
    assert vacancy.reload.published?
  end

  test 'archive on moderate' do
    vacancy = vacancies(:on_moderate)

    patch archive_vacancy_path(vacancy)

    assert_response :redirect
    assert vacancy.reload.archived?
  end

  test 'archive published' do
    vacancy = vacancies(:published)

    patch archive_vacancy_path(vacancy)

    assert vacancy.reload.archived?
    assert_response :redirect
  end

  test 'refute publish archived' do
    vacancy = vacancies(:archived)

    patch publish_vacancy_path(vacancy)

    assert vacancy.reload.archived?
  end
end
