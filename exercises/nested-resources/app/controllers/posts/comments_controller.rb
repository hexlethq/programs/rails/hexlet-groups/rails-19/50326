# frozen_string_literal: true

class Posts::CommentsController < ApplicationController
  before_action :set_post
  before_action :set_comment, only: %i[edit update destroy]

  def edit; end

  def create
    @comment = Post::Comment.new(comment_params)
    @comment.post = @post

    if @comment.save
      redirect_to @post, notice: 'Comment was successfully added'
    else
      # redirect_to @post, alert: 'Comment was not added'
      render 'posts/show'
    end
  end

  def update
    if @comment.update(comment_params)
      redirect_to @post, notice: 'Comment was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @comment.destroy

    redirect_to @post, notice: 'Comment was successfully destroyed.'
  end

  private

  def set_post
    @post = Post.find(params[:post_id])
  end

  def set_comment
    @comment = @post.comments.find(params[:id])
  end

  def comment_params
    params.require(:post_comment).permit(:body)
  end
end
