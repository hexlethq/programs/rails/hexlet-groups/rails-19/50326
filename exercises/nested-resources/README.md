# Nested resources

## Ссылки

* [Nested Resources routing](https://guides.rubyonrails.org/routing.html#nested-resources)
* [The Types of Associations](https://guides.rubyonrails.org/association_basics.html#the-types-of-associations)

## Задачи

### CRUD

Создайте CRUD вложенной модели комментария (Comment). У комментария должна быть связь с постом. У каждого комментария есть ссылка на страницу редактирования и ссылка на удаление (с подтверждением). Отдельных страниц просмотра комментария и списка нет.

### exercises/nested-resources/app/models/post.rb

Добавьте в модель Post связь с комментариями.

### config/routes.rb

Напишите ресурсные роуты для поста и для вложенного ресурса комментария.

### app/views/posts/show.html.slim

Добавьте на страницу просмотра поста форму создания комментариев и список созданных под этим постов комментариев. У каждого комментария есть ссылка для удаления. При нажатии на ссылку появляется подтверждение.
