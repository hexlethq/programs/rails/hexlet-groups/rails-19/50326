# frozen_string_literal: true

require 'test_helper'

class Posts::CommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @post = posts(:one)
    @comment = post_comments(:one)
  end

  test '#create' do
    comment_attr = { body: 'Comment 1' }

    assert_difference('Post::Comment.count') do
      post post_comments_path(@post), params: { post_comment: comment_attr }
    end

    comment = Post::Comment.last

    assert_equal comment.post_id, @post.id
    assert_equal comment.body, comment_attr[:body]
    assert_redirected_to post_path(@post)
  end

  test '#edit' do
    get edit_post_comment_path(@post, @comment)
    assert_response :success
  end

  test '#update' do
    comment_attr = { body: 'Updated comment' }
    patch post_comment_path(@post, @comment), params: { post_comment: comment_attr }

    assert_not_equal comment_attr[:body], @comment.body
    assert_redirected_to post_path(@post)
  end

  test '#destroy' do
    assert_difference('Post::Comment.count', -1) do
      delete post_comment_path(@post, @comment)
    end

    assert_redirected_to post_path(@post)
  end
end
