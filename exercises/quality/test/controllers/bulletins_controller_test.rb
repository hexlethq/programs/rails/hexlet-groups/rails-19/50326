# frozen_string_literal: true

require 'test_helper'

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  test 'get list bulletins' do
    get bulletins_path
    assert_response :success
  end

  test 'show bulletin' do
    bulletin = bulletins(:two)

    get bulletin_path(bulletin)
    assert_response :success
  end
end
