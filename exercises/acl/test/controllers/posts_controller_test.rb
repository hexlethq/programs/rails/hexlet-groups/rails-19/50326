# frozen_string_literal: true

require 'test_helper'

class PostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @post = posts(:one)
  end

  test '#index' do
    get posts_path
    assert_response :success
  end

  test '#new (signed in user)' do
    sign_in_as :one

    get new_post_path
    assert_response :success
  end

  test '#new (guest should raise error)' do
    assert_raises(Pundit::NotAuthorizedError) do
      get new_post_path
    end
  end

  test '#create (signed in user)' do
    user = sign_in_as :one

    post_attr = {
      title: 'title',
      body: 'body'
    }

    assert_difference('Post.count', +1) do
      post posts_path, params: { post: post_attr }
    end

    post = Post.last
    assert_equal post.author, user
    assert_equal post.title, post_attr[:title]
  end

  test '#create (guest should raise error)' do
    post_attr = {
      title: 'title',
      body: 'body'
    }

    assert_raises(Pundit::NotAuthorizedError) do
      post posts_path, params: { post: post_attr }
    end
  end

  test '#show' do
    get post_path posts(:one)
    assert_response :success
  end

  test '#edit (author can edit post)' do
    sign_in_as :one

    get edit_post_path @post
    assert_response :success
  end

  test '#edit (admin can edit post)' do
    sign_in_as :admin

    get edit_post_path @post
    assert_response :success
  end

  test '#edit (non author should raise error)' do
    sign_in_as :two

    assert_raises(Pundit::NotAuthorizedError) do
      get edit_post_path @post
    end
  end

  test '#update (author can update post)' do
    sign_in_as :one

    post_attr = {
      title: 'new title',
      body: 'new body'
    }

    patch post_path @post, params: { post: post_attr }

    @post.reload
    assert_equal @post.title, post_attr[:title]
    assert_equal @post.body, post_attr[:body]
  end

  test '#update (admin can update all posts)' do
    sign_in_as :admin

    post_attr = {
      title: 'new title'
    }

    patch post_path @post, params: { post: post_attr }

    @post.reload
    assert_equal @post.title, post_attr[:title]
  end

  test '#update (non author should raise error)' do
    sign_in_as :two

    assert_raises(Pundit::NotAuthorizedError) do
      post_attr = {
        title: 'new title',
        body: 'new body'
      }

      patch post_path @post, params: { post: post_attr }
    end
  end

  test '#destroy (only admin can delete post)' do
    sign_in_as :admin

    assert_difference('Post.count', -1) do
      delete post_path @post
    end
  end

  test '#destroy (non admin should raise error)' do
    sign_in_as :two

    assert_raises(Pundit::NotAuthorizedError) do
      delete post_path @post
    end
  end
end
