# frozen_string_literal: true

class PostPolicy < ApplicationPolicy
  # BEGIN
  def new?
    user
  end

  def create?
    user
  end

  def edit?
    author_or_admin?
  end

  def update?
    author_or_admin?
  end

  def destroy?
    user&.admin?
  end

  private

  def author_or_admin?
    record.user_id == user&.id || user&.admin?
  end
  # END
end
