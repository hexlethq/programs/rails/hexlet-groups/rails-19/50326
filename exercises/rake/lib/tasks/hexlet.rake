# frozen_string_literal: true

require 'csv'

namespace :hexlet do
  desc 'Import users from csv'
  task :import_users, [:path] => :environment do |_t, args|
    path = args[:path]

    CSV.foreach(path, headers: true) do |row|
      user = row.to_h
      User.create user
    end
  end
end
