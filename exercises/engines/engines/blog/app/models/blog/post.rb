# frozen_string_literal: true

module Blog
  class Post < ApplicationRecord
    validates :title, presence: true
    validates :body, presence: true
  end
end
