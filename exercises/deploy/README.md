# Deploy

## Ссылки

* [Гайд - Что такое деплой?](https://guides.hexlet.io/deploy/)
* [Hexlet CV](https://github.com/Hexlet/hexlet-cv)
* [Документация Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)
* [Getting Started on Heroku with Rails 6.x - deploy to Heroku](https://devcenter.heroku.com/articles/getting-started-with-rails6#deploy-your-application-to-heroku)

## Задачи

Копировать приложение в директорию домашней работы не нужно.

* Установите Heroku CLI
* Склонируйте к себе проект hexlet-cv и запустите локально следуя инструкциям в README
* Создайте приложение Heroku. После создания приложения появится удаленный репозиторий `heroku`

  ```sh
  heroku create <app_name>
  git remote -v
  ```

* Задайте окружение для приложения

  ```sh
  heroku config:set RAILS_ENV=production
  heroku config:set HOST=<app_name>.herokuapp.com
  heroku config:set EMAIL_FROM=<your_email>

  ```

* Создайте базу данных (Postgres). Вместе с созданной базой данных будет создана переменная окружения `DATABASE_URL`, которая содержит данные для подключения к базе данных

  ```sh
  heroku addons:create heroku-postgresql:hobby-dev
  heroku config # Вывод переменных окружений
  ```

* Деплой приложения на Heroku выполняется пушем в удаленный репозиторий. Выполните деплой командой

  ```sh
  # git push heroku
  make deploy

  # See http://<app_name>.herokuapp.com
  ```

* Когда будете отправлять домашнее задание на проверку, приложите ссылку на задеплоенное приложение.

## Дополнительное задание

Подключите к приложению Rollbar и убедитесь, что сообщения об ошибках отправляются в сервис.
