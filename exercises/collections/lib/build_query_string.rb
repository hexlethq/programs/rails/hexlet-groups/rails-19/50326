# frozen_string_literal: true

# BEGIN
def build_query_string(params)
  params
    .sort
    .map { |pair| pair.join('=') }
    .join('&')
end
# END
