# frozen_string_literal: true

def make_censored(text, stop_words)
  # BEGIN
  grawlixes = '$#%!'

  text
    .split
    .map { |word| stop_words.include?(word) ? grawlixes : word }
    .join(' ')
  # END
end
