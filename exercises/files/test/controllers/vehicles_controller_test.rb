# frozen_string_literal: true

require 'test_helper'

class VehiclesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @vehicle = vehicles(:one)
  end

  test '#index' do
    get vehicles_path
    assert_response :success
  end

  test '#new' do
    get new_vehicle_path
    assert_response :success
  end

  test '#create' do
    assert_difference('Vehicle.count') do
      post vehicles_path, params: {
        vehicle: {
          manufacture: 'manufacture 1',
          model: 'model 1',
          color: 'color 1',
          doors: '2',
          kilometrage: '10000',
          production_year: '01-01-2000',
          image: fixture_file_upload('hexlet.png', 'image/png')
        }
      }
    end
  end

  test '#show' do
    get vehicle_path(@vehicle)
    assert_response :success
  end

  test '#edit' do
    get edit_vehicle_path(@vehicle)
    assert_response :success
  end

  test '#update' do
    patch vehicle_path(@vehicle), params: {
      vehicle: {
        manufacture: 'new manufacture',
        model: 'new model',
        color: 'new color',
        doors: '3',
        kilometrage: '10000',
        production_year: '01-01-2000',
        image: fixture_file_upload('hexlet.png', 'image/png')
      }
    }
  end

  test '#destroy' do
    assert_difference('Vehicle.count', -1) do
      delete vehicle_path(@vehicle)
    end

    assert_redirected_to vehicles_path
  end
end
