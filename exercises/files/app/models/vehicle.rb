# frozen_string_literal: true

class Vehicle < ApplicationRecord
  # BEGIN
  has_one_attached :image

  validates :image,
            attached: true,
            size: { less_than_or_equal_to: 5.megabyte },
            content_type: %i[jpg jpeg png]
  # END
end
