# frozen_string_literal: true

require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task = tasks(:one)
  end

  test 'should get index' do
    get tasks_url
    assert_response :success
  end

  test 'should get new' do
    get new_task_url
    assert_response :success
  end

  test 'should create task' do
    assert_difference('Task.count') do
      attrs = {
        name: @task.name,
        description: @task.description,
        user_id: @task.user_id,
        status_id: @task.status_id
      }
      post tasks_url, params: { task: attrs }
    end

    assert_redirected_to task_url(Task.last)
  end

  test 'should show task' do
    get task_url(@task)
    assert_response :success
  end

  test 'should get edit' do
    get edit_task_url(@task)
    assert_response :success
  end

  test 'should update task' do
    new_task = tasks(:two)
    attrs = {
      name: new_task.name,
      description: new_task.description,
      user_id: @task.user_id,
      status_id: new_task.status_id
    }
    patch task_url(@task), params: { task: attrs }
    assert_redirected_to task_url(@task)
  end

  test 'should destroy task' do
    assert_difference('Task.count', -1) do
      delete task_url(@task)
    end

    assert_redirected_to tasks_url
  end
end
