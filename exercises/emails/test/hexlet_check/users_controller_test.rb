# frozen_string_literal: true

require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  test 'new' do
    get new_user_url
    assert_response :success
  end

  test 'create' do
    assert_difference 'User.count', 1 do
      post users_url, params: { user: {
        name: 'Test',
        email: 'test@test.io',
        password: 'password',
        password_confirmation: 'password'
      } }
    end

    assert_response :redirect

    user = User.last

    assert user.waiting_confirmation?
  end

  test 'show' do
    user = sign_in_as :one

    get user_url(user)
    assert_response :success
  end

  test 'confirm' do
    user = users :waiting_confirmation

    # binding.irb

    get confirm_user_url(confirmation_token: user.confirmation_token)
    assert_response :redirect

    user.reload

    assert user.active?
  end
end
