# frozen_string_literal: true

require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  test '#new' do
    get new_user_path
    assert_response :success
  end

  test '#create' do
    assert_difference('User.count', +1) do
      post users_path, params: {
        user: {
          name: 'user name',
          email: 'test@test.io',
          password: 'password',
          password_confirmation: 'password'
        }
      }
    end

    user = User.last

    assert_redirected_to user
    assert user.waiting_confirmation?
  end

  test '#show' do
    user = sign_in_as :one

    get user_path(user)
    assert_response :success
  end

  test '#confirm' do
    user = users :waiting_confirmation

    get confirm_user_path(confirmation_token: user.confirmation_token)
    assert_redirected_to user

    user.reload

    assert user.active?
  end
end
