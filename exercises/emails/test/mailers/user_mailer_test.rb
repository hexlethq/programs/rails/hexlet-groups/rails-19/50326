# frozen_string_literal: true

require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  test 'account activation' do
    user = users(:one)
    mail = UserMailer.with(user: user).account_activation

    assert_equal [user.email], mail.to
    assert_equal I18n.t('subject', scope: 'user_mailer.account_activation'), mail.subject
    assert_match I18n.t('welcome_text', scope: 'user_mailer.account_activation'), mail.body.encoded
    assert_match user.confirmation_token, mail.body.encoded
  end
end
