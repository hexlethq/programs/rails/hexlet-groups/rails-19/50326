# frozen_string_literal: true

require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  test '#index' do
    get tasks_path
    assert_response :success
  end

  test '#new' do
    get new_task_path

    assert_response :success
  end

  test '#show' do
    task = tasks(:one)
    get task_path(task)

    assert_response :success
  end

  test '#edit' do
    task = tasks(:one)
    get edit_task_path(task)

    assert_response :success
  end

  test '#create' do
    attrs = {
      name: 'Name task 1',
      description: 'Task description',
      status: 'in progress',
      creator: 'Me',
      performer: 'Not me',
      completed: false
    }

    post tasks_path({ task: attrs })

    task = Task.find_by(name: attrs[:name])

    assert task
    assert_redirected_to task_path(task)
  end

  test '#update' do
    old_task = tasks(:one)
    attrs = {
      name: 'Name task 42',
      completed: true
    }

    patch task_path(old_task), params: { task: attrs }

    task = Task.find(old_task[:id])

    assert task
    assert_redirected_to task_path(task)
    assert_equal task.name, attrs[:name]
    assert_equal task.completed, attrs[:completed]
  end

  test '#destroy' do
    task = tasks(:two)

    delete task_path(task)

    assert_redirected_to tasks_path
    assert_not_equal tasks.count, Task.count
  end
end
