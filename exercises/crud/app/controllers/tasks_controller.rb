# frozen_string_literal: true

class TasksController < ApplicationController
  def index
    @tasks = Task.all
  end

  def new
    @task = Task.new
  end

  def create
    @task = Task.new(task_params)
    if @task.save
      redirect_to task_path(@task)
    else
      render :new
    end
  end

  def show
    @task = Task.find(params[:id])
  end

  def edit
    @task = Task.find(params[:id])
  end

  def update
    @task = Task.find(params[:id])
    if @task.update(task_params)
      flash[:notice] = 'Task has been successfully updated'
      redirect_to task_path(@task)
    else
      flash.now[:notice] = 'Tash has not been updated'
      render :edit
    end
  end

  def destroy
    @task = Task.find(params[:id])
    if @task.destroy
      flash[:notice] = 'Task has been successfully removed'
      redirect_to tasks_path
    else
      flash.now[:notice] = 'Tash has not been removed'
      redirect_to task_path(params[:id])
    end
  end

  private

  def task_params
    params.require(:task).permit(:name,
                                 :description,
                                 :status,
                                 :creator,
                                 :performer,
                                 :completed)
  end
end
