# frozen_string_literal: true

class Router
  def call(env)
    req = Rack::Request.new(env)

    case req.path_info
    when '/'
      [200, {}, 'Hello, World!']
    when '/about'
      [200, {}, 'About page']
    else
      [404, {}, '404 Not Found']
    end
  end
end
