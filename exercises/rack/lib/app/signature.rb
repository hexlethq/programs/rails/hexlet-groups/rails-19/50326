# frozen_string_literal: true

require 'digest'

class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    digest = Digest::SHA2.new(256)
    code, headers, body = @app.call(env)
    content = "#{body}\n #{digest.hexdigest(body)}"

    [code, headers, content]
  end
end
