# frozen_string_literal: true

class ExecutionTimer
  def initialize(app)
    @app = app
  end

  def call(env)
    start = Time.now.utc.usec
    code, headers, body = @app.call(env)
    finish = Time.now.utc.usec
    diff = finish - start

    [code, headers, "#{body}\n #{diff}"]
  end
end
