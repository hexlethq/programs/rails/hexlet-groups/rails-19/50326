# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.version = '0.1.0'
  spec.name    = 'testing-2'
  spec.summary = 'Full Rails'
  spec.authors = ['Hexlet']
  spec.files   = `git ls-files`
end
