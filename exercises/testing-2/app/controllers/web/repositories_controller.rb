# frozen_string_literal: true

# BEGIN
require 'octokit'
# END

class Web::RepositoriesController < Web::ApplicationController
  def index
    @repositories = Repository.all
  end

  def new
    @repository = Repository.new
  end

  def show
    @repository = Repository.find params[:id]
  end

  def create
    # BEGIN
    repository_meta = Octokit::Repository.from_url(permitted_params[:link])
    client = Octokit::Client.new
    data = client.repository(repository_meta)

    @repository = Repository.new(
      link: data.html_url,
      owner_name: data.owner.login,
      repo_name: data.name,
      description: data.description,
      default_branch: data.default_branch,
      watchers_count: data.watchers,
      language: data.language,
      repo_created_at: data.created_at,
      repo_updated_at: data.updated_at
    )

    if @repository.save
      redirect_to repository_path(@repository), notice: t('success')
    else
      render :new, notice: t('fail')
    end
    # END
  end

  def edit
    @repository = Repository.find params[:id]
  end

  def update
    @repository = Repository.find params[:id]

    if @repository.update(permitted_params)
      redirect_to repositories_path, notice: t('success')
    else
      render :edit, notice: t('fail')
    end
  end

  def destroy
    @repository = Repository.find params[:id]

    if @repository.destroy
      redirect_to repositories_path, notice: t('success')
    else
      redirect_to repositories_path, notice: t('fail')
    end
  end

  private

  def permitted_params
    params.require(:repository).permit(:link)
  end
end
