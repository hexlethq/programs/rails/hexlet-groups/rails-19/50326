# frozen_string_literal: true

require 'test_helper'

class Web::RepositoriesControllerTest < ActionDispatch::IntegrationTest
  # BEGIN
  test 'should_create' do
    repo_url = 'https://github.com/octocat/Hello-World'
    api_repo_url = 'https://api.github.com/repos/octocat/Hello-World'
    response = load_fixture('files/response.json')

    stub_request(:get, api_repo_url)
      .to_return(
        status: 200,
        body: response,
        headers: { 'Content-Type': 'application/json' }
      )

    assert_difference('Repository.count', +1) do
      post repositories_path, params: {
        repository: {
          link: repo_url
        }
      }
    end

    repo = Repository.last
    assert_equal repo.link, repo_url
  end
  # END
end
