# frozen_string_literal: true

require 'test_helper'

class Api::UsersControllerTest < ActionDispatch::IntegrationTest
  # BEGIN
  test '#index' do
    get api_users_path, as: :json
    assert_response :success
  end

  test '#show' do
    get api_user_path(users(:one)), as: :json
    assert_response :success
  end
  # END
end
