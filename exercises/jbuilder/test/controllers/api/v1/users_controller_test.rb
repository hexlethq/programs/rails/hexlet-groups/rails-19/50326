# frozen_string_literal: true

require 'test_helper'

class Api::V1::UsersControllerTest < ActionDispatch::IntegrationTest
  test '#index' do
    get api_v1_users_path, as: :json
    assert_response :success
  end

  test '#show' do
    get api_v1_user_path(users(:one)), as: :json
    assert_response :success
  end
end
