# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  return '' if start > stop

  result = ''

  start.upto(stop) do |num|
    result = if num.modulo(3).zero? && num.modulo(5).zero?
               "#{result} FizzBuzz"
             elsif num.modulo(3).zero?
               "#{result} Fizz"
             elsif num.modulo(5).zero?
               "#{result} Buzz"
             else
               "#{result} #{num}"
             end
  end

  result.strip
end
# END
