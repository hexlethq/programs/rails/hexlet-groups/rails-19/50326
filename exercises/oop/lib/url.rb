# frozen_string_literal: true

# BEGIN
require 'uri'
require 'forwardable'

# BEGIN
class Url
  include Comparable
  extend Forwardable

  attr_reader :query_params

  def_delegators :@url, :scheme, :host, :query

  def initialize(url)
    @url = URI(url)
    @query_params = query_to_obj(query)
  end

  def query_param(key, default = nil)
    @query_params.fetch(key, default)
  end

  def to_s
    @url.to_s
  end

  def <=>(other)
    to_s <=> other.to_s
  end

  private

  def query_to_obj(query_str)
    return {} if query_str.nil?

    query_str
      .split('&')
      .map { |str| str.split('=') }
      .to_h
      .transform_keys(&:to_sym)
  end
end
# END
