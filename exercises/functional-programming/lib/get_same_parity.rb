# frozen_string_literal: true

# BEGIN
def get_same_parity(list)
  list.select { |num| list.first.even? == num.even? }
end
# END-
