# frozen_string_literal: true

# BEGIN
def anagramm_filter(word, check_list)
  chars = word.chars.sort

  check_list.filter { |check_word| check_word.chars.sort == chars }
end
# END
