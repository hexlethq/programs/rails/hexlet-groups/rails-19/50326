# frozen_string_literal: true

require 'csv'

class Web::UsersController < Web::ApplicationController
  before_action :set_user, only: %i[show edit update destroy]

  def index
    @users = User.page(params[:page])
  end

  def show; end

  def new
    @user = User.new
  end

  def edit; end

  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to @user, notice: t('success')
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @user.update(user_params)
      redirect_to @user, notice: t('success')
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @user.destroy

    redirect_to users_url, notice: t('success')
  end

  def normal_csv
    respond_to do |format|
      format.csv do
        csv = generate_csv(User.column_names, User.all)
        send_data(csv)
      end
    end
  end

  def stream_csv
    # BEGIN
    headers.delete('Content-Length')
    headers['Cache-Control'] = 'no-cache'
    headers['Content-Type'] = 'text/csv'
    headers['Content-Disposition'] = 'attachment; filename="users.csv"'

    self.response_body = generate_stream_csv(User.column_names)
    # END
  end

  private

  def generate_csv(column_names, records)
    CSV.generate do |csv|
      csv << column_names # add headers to the CSV

      records.find_each do |record|
        csv << record.attributes.values_at(*column_names)
      end
    end
  end

  # BEGIN
  def generate_stream_csv(column_names)
    Enumerator.new do |yielder|
      yielder << CSV.generate_line(column_names)

      User.find_each do |row|
        yielder << CSV.generate_line(
          row.attributes.values_at(*column_names)
        )
      end
    end
  end
  # END

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(
      :name,
      :email
    )
  end
end
