# frozen_string_literal: true

require 'test_helper'

module HexletCheck
  module Posts
    class CommentsControllerTest < ActionDispatch::IntegrationTest
      setup do
        @comment = post_comments(:one)
        @post = @comment.post
      end

      test 'should create post' do
        assert_difference('Post::Comment.count') do
          post post_comments_url(@post, locale: :en), params: { post_comment: {
            body: 'test'
          } }
        end

        assert_redirected_to post_url(@post)
      end

      test 'should destroy post' do
        assert_difference('Post::Comment.count', -1) do
          delete post_comment_url(@post, @comment, locale: :en)
        end

        assert_redirected_to post_url(@post)
      end
    end
  end
end
