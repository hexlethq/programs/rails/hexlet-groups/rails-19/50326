# frozen_string_literal: true

require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  def test_about_page
    get page_path :about
    assert_response :success
  end
end
