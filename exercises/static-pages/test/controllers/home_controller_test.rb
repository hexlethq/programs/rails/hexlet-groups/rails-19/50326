# frozen_string_literal: true

require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  def test_root
    get root_path
    assert_response :success
  end
end
