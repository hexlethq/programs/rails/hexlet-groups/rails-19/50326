# frozen_string_literal: true

# BEGIN
module Model
  def self.included(base)
    base.extend(ClassMethods)
  end

  def initialize(attributes = {})
    @attributes = self.class.scheme.to_a.map do |(name, options)|
      value = attributes.fetch(name, options[:default])
      converted = self.class.convert_value(value, options[:type])
      [name, converted]
    end.to_h
  end

  def attributes
    @attributes
  end

  module ClassMethods
    CONVERTERS = {
      string: ->(value) { value.to_s },
      integer: ->(value) { value.to_i },
      boolean: ->(value) { value != false && !value.nil? },
      datetime: ->(value) { DateTime.parse(value) }
    }.freeze

    def convert_value(value, type)
      return if value.nil?
      return value if type.nil?

      converter = CONVERTERS[type]
      converter.call value
    end

    def scheme
      @scheme
    end

    def attribute(name, options = {})
      @scheme ||= {}
      @scheme[name] = options

      define_method name do
        @attributes[name]
      end

      define_method "#{name}=" do |value|
        @attributes[name] = self.class.convert_value(value, options[:type])
      end
    end
  end
end
# END
