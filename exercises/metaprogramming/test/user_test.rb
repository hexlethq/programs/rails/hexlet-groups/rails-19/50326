# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/user'

class UserTest < Minitest::Test
  # BEGIN
  def test_user
    attributes = { name: 'Mike', birthday: '01/02/1993', active: true }
    result_attributes = { name: 'Mike', birthday: DateTime.parse('01/02/1993'), active: true }

    user = User.new(attributes)
    assert_equal result_attributes, user.attributes
  end

  def test_default_attributes
    attributes = { birthday: '01/02/1993' }
    result_attributes = { name: 'Andrey', birthday: DateTime.parse('01/02/1993'), active: false }

    user = User.new(attributes)
    assert_equal result_attributes, user.attributes
  end

  def test_users_not_equal
    user1 = User.new name: 'Mike'
    user2 = User.new name: 'Maks'

    assert_equal 'Mike', user1.name
    assert_equal 'Maks', user2.name
    refute_equal user1.name, user2.name
  end

  def test_attribute_convert
    user = User.new birthday: '01/01/2021', active: 'yes'
    result_attributes = { name: 'Andrey', birthday: DateTime.parse('01/01/2021'), active: true }

    assert_equal result_attributes, user.attributes
    assert_instance_of TrueClass, user.active
  end
  # END
end
