# frozen_string_literal: true

require 'open-uri'

class Hacker
  class << self
    def hack(email, password)
      # BEGIN
      host = 'https://rails-l4-collective-blog.herokuapp.com'

      sign_up_url = URI.join(host, '/users/', 'sign_up')
      users_url = URI.join(host, 'users')

      res = Net::HTTP.get_response(sign_up_url)

      html = Nokogiri::HTML(res.body)
      token = html.at_css('input[name="authenticity_token"]')['value']
      cookie = res['set-cookie']

      headers = { cookie: cookie, 'Content-Type': 'application/json' }
      body = {
        authenticity_token: token,
        user: {
          email: email,
          password: password,
          password_confirmation: password

        }
      }

      Net::HTTP.post(users_url, body.to_json, headers)
      # END
    end
  end
end
