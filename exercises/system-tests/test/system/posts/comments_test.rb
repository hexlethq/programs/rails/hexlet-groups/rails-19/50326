# frozen_string_literal: true

require 'application_system_test_case'

class Posts::CommentsTest < ApplicationSystemTestCase
  setup do
    @post = posts(:one)
    @comment = post_comments(:one)
  end

  test 'create comment' do
    visit post_path @post

    assert_selector 'form#new_post_comment'

    assert_difference('Post::Comment.count', +1) do
      find('textarea', id: 'post_comment_body').fill_in with: 'New comment'
      click_on 'Create Comment'
    end

    comment = @post.comments.last

    assert comment
    assert_equal comment.body, 'New comment'
  end

  test 'update comment' do
    visit edit_post_comment_path @post, @comment

    assert_selector 'h1', text: 'Editing comment'
    assert_selector 'form'

    fill_in 'Body', with: 'updated comment body'
    click_on 'Update Comment'

    assert_equal @comment.reload.body, 'updated comment body'
  end

  test 'delete comment' do
    visit post_path @post

    assert_difference('Post::Comment.count', -1) do
      accept_confirm do
        find('.card', text: @comment.body).click_link 'Delete'
      end

      assert_current_path post_path @post
    end
  end
end
