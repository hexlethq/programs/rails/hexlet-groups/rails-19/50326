# frozen_string_literal: true

require 'application_system_test_case'

# BEGIN
class PostsTest < ApplicationSystemTestCase
  setup do
    @post = posts(:without_comments)
  end

  test 'visit inex' do
    visit posts_path

    assert_selector 'h1', text: 'Posts'
  end

  test 'visit post page' do
    visit post_path @post

    assert_selector 'h1', text: @post.title
  end

  test 'create post' do
    visit new_post_path

    assert_selector 'h1', text: 'New post'
    assert_selector 'form'

    assert_difference('Post.count', +1) do
      fill_in 'Title', with: 'title'
      fill_in 'Body', with: 'body'
      click_on 'Create Post'
    end

    post = Post.last

    assert_equal post.title, 'title'
    assert_equal post.body, 'body'
  end

  test 'edit post' do
    visit edit_post_path @post

    assert_selector 'h1', text: 'Editing post'
    assert_selector 'form'

    fill_in 'Title', with: 'new title'
    fill_in 'Body', with: 'new body'
    click_on 'Update Post'

    assert_equal @post.reload.title, 'new title'
    assert_equal @post.reload.body, 'new body'
  end

  test 'destroy post' do
    visit posts_path

    assert_difference('Post.count', -1) do
      accept_confirm do
        find('tr', text: @post.title).click_link 'Destroy'
      end

      assert_current_path posts_path
    end
  end
end
# END
