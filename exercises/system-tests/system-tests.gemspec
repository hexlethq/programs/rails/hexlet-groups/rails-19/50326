# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.version = '0.1.0'
  spec.name    = 'system-tests'
  spec.summary = 'Real Rails'
  spec.authors = ['Hexlet']
  spec.files   = `git ls-files`
end
