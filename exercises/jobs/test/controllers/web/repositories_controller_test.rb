# frozen_string_literal: true

require 'test_helper'

class Web::RepositoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @repo = repositories :created
    @repo_link = 'https://github.com/github/github'
  end

  test '#index' do
    get repositories_path
    assert_response :success
  end

  test '#new' do
    get new_repository_path
    assert_response :success
  end

  test '#create' do
    assert_difference('Repository.count', 1) do
      post repositories_path, params: {
        repository: {
          link: @repo_link
        }
      }
    end

    repository = Repository.find_by(link: @repo_link)

    assert_redirected_to repository_path(repository)
    assert repository.present?
    assert_enqueued_with job: RepositoryLoaderJob
  end

  test '#update' do
    patch repository_path(@repo)

    assert_redirected_to repositories_path
    assert_enqueued_with job: RepositoryLoaderJob
  end

  test '#update_repos' do
    patch update_repos_repositories_path

    assert_redirected_to repositories_path

    assert_enqueued_with job: RepositoryLoaderJob
  end

  test '#destroy' do
    assert_difference('Repository.count', -1) do
      delete repository_path(@repo)
    end

    assert_redirected_to repositories_path
  end
end
