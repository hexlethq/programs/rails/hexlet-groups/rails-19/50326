# frozen_string_literal: true

require 'test_helper'

module HexletCheck
  class Web::RepositoriesControllerTest < ActionDispatch::IntegrationTest
    setup do
      @repo = repositories :created
      @repo_link = 'https://github.com/github/github'
    end

    test 'get index' do
      get repositories_url
      assert_response :success
    end

    test 'get new' do
      get new_repository_url
      assert_response :success
    end

    test 'create' do
      assert_difference 'Repository.count', 1 do
        post repositories_url, params: {
          repository: {
            link: @repo_link
          }
        }
      end

      repository = Repository.find_by(link: @repo_link)

      assert_response :redirect
      assert repository.present?
      assert_enqueued_with job: RepositoryLoaderJob
    end

    test 'update' do
      patch repository_url(@repo)

      assert_response :redirect
      assert_enqueued_with job: RepositoryLoaderJob
    end

    test 'destroy' do
      assert_difference 'Repository.count', -1 do
        delete repository_url(@repo)
      end

      assert_response :redirect
    end

    test 'update_repos' do
      patch update_repos_repositories_url

      assert_response :redirect

      assert_enqueued_with job: RepositoryLoaderJob
    end
  end
end
