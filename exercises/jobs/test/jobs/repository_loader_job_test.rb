# frozen_string_literal: true

require 'test_helper'

class RepositoryLoaderJobTest < ActiveJob::TestCase
  test '#create' do
    repo = repositories :created
    api_repo_url = 'https://api.github.com/repos/Hexlet/hexlet-cv'

    response = load_fixture('files/response.json')

    stub_request(:get, api_repo_url)
      .to_return(
        status: 200,
        body: response,
        headers: { 'Content-Type' => 'application/json' }
      )

    RepositoryLoaderJob.perform_now repo.id
    repo.reload

    assert_equal repo.owner_name, 'octocat'
    assert repo.fetched?
  end

  test '#update' do
    repo = repositories :fetched
    api_repo_url = 'https://api.github.com/repos/Hexlet/hexlet-sicp'

    response = load_fixture('files/response.json')

    stub_request(:get, api_repo_url)
      .to_return(
        status: 200,
        body: response,
        headers: { 'Content-Type' => 'application/json' }
      )

    old_description = repo.description

    RepositoryLoaderJob.perform_now repo.id
    repo.reload

    assert_not_equal old_description, repo.description
    assert repo.fetched?
  end
end
