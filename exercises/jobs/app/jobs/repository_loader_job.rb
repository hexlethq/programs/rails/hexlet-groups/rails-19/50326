# frozen_string_literal: true

require 'octokit'

class RepositoryLoaderJob < ApplicationJob
  queue_as :repository

  def perform(repository_id)
    repository = Repository.find(repository_id)
    repository.fetch!

    repo_meta = Octokit::Repository.from_url(repository.link)
    client = Octokit::Client.new
    data = client.repository(repo_meta)

    repository.owner_name = data.owner.login
    repository.repo_name = data.name
    repository.description = data.description
    repository.default_branch = data.default_branch
    repository.watchers_count = data.watchers
    repository.language = data.language
    repository.repo_created_at = data.created_at
    repository.repo_updated_at = data.updated_at
    repository.done
    repository.save!
  rescue StandardError
    repository.fail!
  end
end
